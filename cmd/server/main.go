package main

import (
	"encoding/json"
	"fmt"
	"os"
)

func main() {
	_, err := json.Marshal("Test")
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	fmt.Println("Starting")
}
